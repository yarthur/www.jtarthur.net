# jtarthur.net Changelog

All notable changes to [www.jtarthur.net][home] can be found listed here.

This changelog follows conventions outlined in <http://keepachangelog.com>. The
project adheres to [Semantic Versioning conventions](http://semver.org).

## 0.2.2 - 2020-02-12

- Update README to a more accurate reflection of the current project.
- Enable Prettier for MD.

### Fixed

- Implement the license correctly.

## 0.2.1 - 2020-02-12

### Changed

- Update my location in the home page copy.

### Fixed

- Fix a mixed-content issue.

## 0.2.0 - 2020-02-12

### Added

- Enable CI functionality

## 0.1.1 - 2020-02-09

### Fixed

- Fix some navigation issues

## 0.1.0 - 2019-11-26

### Added

- Add bare minimum pages for the site so far.

[home]: https://www.jtarthur.net
