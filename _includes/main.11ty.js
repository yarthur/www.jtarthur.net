<!DOCTYPE html>
	<html>
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=yes" />
	<title>${data.title} | John Arthur</title>
	<link type="image/x-icon" rel="shortcut icon" href="http://jtarthur.net/images/layout/favicon.png" />
	<link rel="stylesheet" href="http://jtarthur.net/assets/css/syntax.css">
	<link rel="stylesheet" href="http://jtarthur.net/assets/css/main.css">
	<link rel="stylesheet" href="http://jtarthur.net/assets/css/print.css" media="print" />
	</head>
	<body class="www-jtarthur-net">
	<div role="banner">
	<a href="/" title=" [http://jtarthur.net]"><img src="http://jtarthur.net/assets/img/layout/site_id.png" alt="" /></a>
	</div>
	<ul role="navigation" class="nav-main">
	${data.core.navigation.map(function({ url, label, title = label }) {
		return `<li><a href="/${url}" title="${title}">${label}</a></li>`;
	})}
${generateNav(data.core.navigation)}
	</ul>
	<div role="main">
	${data.content}
	</div>
	<div role="contentinfo">
	<address class="vcard h-card contact">
	<a class="fn org url p-name p-org u-url" href="http://jtarthur.net" title="John Arthur [www.jtarthur.net]" rel="me"><span class="given-name p-given-name">John</span> <span class="family-name p-family-name">Arthur</span></a>
	|
	<span class="adr p-adr">
	<span class="locality p-locality">Lincoln</span>,
	<span class="region p-region">Nebraska</span>
	</span>
	|
	<span class="email u-email">john@jtarthur.net</span>
	|
	<a href="http://www.linkedin.com/pub/john-arthur/3/b60/591" style="text-decoration:none;">
	<img src="http://s.c.lnkd.licdn.com/scds/common/u/img/webpromo/btn_in_20x15.png" width="20" height="15" alt="View John Arthur's LinkedIn profile" style="vertical-align:middle" border="0" />
	</a>
	</address>
	<p>Design and Content &copy; John Arthur</p>
	</div>
	<!-- build:js /assets/js/jta.js -->
	<!--
	<script src="/assets/js/lib/jquery/jquery.min.js"></script>
	<script src="/assets/js/jta.js"></script>
	-->
	<!-- endbuild -->
	</body>
	</html>
