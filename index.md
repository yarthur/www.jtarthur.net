---
layout: main
---

# Hi! I'm John

I’m a Web Developer in Solon, Iowa. Using HTML, CSS, and JavaScript, I build
professional-grade web sites atop a solid foundation of usability and
accessibility. At this site, you can learn more about [me][about] and [my
skills, knowledge, and experience][experience].

[about]: /about
[experience]: /experience
[contact]: /contact
