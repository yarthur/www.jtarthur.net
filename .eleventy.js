/* eslint-env node */

module.exports = function(eleventyConfig) {
	eleventyConfig.addPassthroughCopy({
		_assets: "assets"
	});
};
