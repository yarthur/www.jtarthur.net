---
layout: main
title: About Me
---

# Allow me to introduce myself.

I was born and raised in Nebraska; before obsessing over semantic markup and
progressive enhancement, I was detasseling corn in the summers, and listening to
Husker football on the radio in the fall. I met my wife, a Minneapolis native,
at Creighton University, and was able to extol upon her the virtues of The Good
Life. For a short while, I moved to the Twin Cities with her, before we returned
to Omaha, where we had our daughter. We then moved to Lincoln, Nebraska, where
my wife earned her PhD from
<abbr title="University of Nebraska at Lincoln">UNL</abbr>, and we added a son
to our family. We recently moved to Solon, Iowa, and look forward to the many
adventures that is sure to bring.

I first started playing with HTML in high school. As mentioned before, I had a
trusty Geocities account, a copy of _HTML For Dummies_, and an Internet
connection. That first site was something of an abomination, with an
embarrassing number of frames, blinking text, lightsaber HRs, and graphic
backgrounds. At the same time, I was hooked by the freedom to create and publish
whatever I felt like. The ability that the Internet has to quickly and easily
allow someone to publish whatever they desire to a nearly infinite audience is
still awe-inspiring to me, and I love seeing what people do with that. It was
that power that led me to this profession, and I haven’t regretted it in the
least.

Beyond HTML, there are plenty of things that keep me occupied. My family is a
big part of my life, and my children are especially adept at keeping me busy
(and youthful — it’s disturbing how well I relate to a toddler's psyche). I love
to spend time in the kitchen, cooking and baking, and then reaping the rewards.
This has also led me to dabbling in homebrewing, and failing at keeping bees for
more than one summer at a time. Finally, I’ve always enjoyed music — I have my
headphones on more often than not, and though I haven’t made time to play my
bass in way too long, I hope I will change that someday soon.
