## www.jtarthur.net

> It's my own website! How exciting!

[![pipeline status](https://gitlab.com/yarthur/www.jtarthur.net/badges/master/pipeline.svg)](https://gitlab.com/yarthur/www.jtarthur.net/-/commits/master)

This is the latest iteration of my Web site.

## Install

**Dependencies:** In order to run this locally, you'll want to have Git and Node
installed on your system. Assuming that's the case, here's how you get things
working...

To install the code needed and get things rolling, follow these simple steps:

1.  Clone the repository locally.

    ```sh
    git clone git@gitlab.com:yarthur/www.jtarthur.net.git
    ```

2.  Install Node dependencies.

    ```sh
    npm i
    ```

3.  To build, you can run the build script. To run a local instance, you can
    also run the serve script.

    ```sh
    # Build it once.
    npm run build

    # Creates a local server instance, watches for changes, and updates the build accordingly.
    npm run serve
    ```

## Maintainer

[John Arthur](https://gitlab.com/yarthur)

## Contribute

This is a personal web site, and I don't intend to branch out to contributed
content.

That said, please feel free to
[log issues](https://gitlab.com/yarthur/www.jtarthur.net/issues) you may find,
such as typos, factual inaccuracies, or performance concerns. I make no
guarantee that I'll address them in a timely manner, but I promise I'll
appreciate the effort.

## License

[ISC](LICENSE) © John Arthur
